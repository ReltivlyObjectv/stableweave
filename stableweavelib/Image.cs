﻿using System;
using System.IO;

namespace stableweavelib {
    public class Image {
        public string Base64 { get; set; }

        public Image(string base64) {
            Base64 = base64;
        }

        public void saveToFile(string filePath) {
            File.WriteAllBytes(filePath, Convert.FromBase64String(Base64));
        }
    }
}
