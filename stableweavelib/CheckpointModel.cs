﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace stableweavelib {
    public class CheckpointModel {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("model_name")]
        public string ModelName { get; set; }
        [JsonProperty("hash")]
        public string Hash { get; set; }
        [JsonProperty("sha256")]
        public string Sha256 { get; set; }
        [JsonProperty("filename")]
        public string FileName { get; set; }
        [JsonProperty("config")]
        public string Config { get; set; }

        public CheckpointModel(string title, string model_name, string hash, string sha256, string filename, string config) {
            Title = title;
            ModelName = model_name;
            Hash = hash;
            Sha256 = sha256;
            FileName = filename;
            Config = config;
        }

        public override string ToString() {
            return "CheckpointModel: " + ModelName;
        }

        public static CheckpointModel[] QueryForCheckpoints(string baseURI) {
            string modelsURI = baseURI + "/sdapi/v1/sd-models";
            Console.WriteLine("Querying for Models at " + modelsURI);
            WebRequest request = HttpWebRequest.Create(modelsURI);
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(responseStream);
            string responseString = streamReader.ReadToEnd();

            CheckpointModel[] deserializedCheckpoints = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckpointModel[]>(responseString);
            Console.WriteLine("Found " + deserializedCheckpoints.Length + " checkpoints");
            return deserializedCheckpoints;
        }
    }
}
