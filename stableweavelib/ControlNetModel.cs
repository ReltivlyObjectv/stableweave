﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace stableweavelib {
    public class ControlNetModel {
        public enum ControlNetModule {
            DEPTH,
            OPENPOSE,
            //TODO Add More controlnet types
            UNKNOWN
        }
        private class ControlNetAPIResponse {
            [JsonProperty("model_list")]
            public string[] modelList;
        }

        public string Name { get; set; }
        public string Hash { get; set; }
        public ControlNetModule Module { get; set; }

        public ControlNetModel(string name, string hash, ControlNetModule module) {
            Name = name;
            Module = module;
            Hash = hash;
        }
        public static ControlNetModel[] QueryForModels(string baseURI) {
            string modelsURI = baseURI + "/controlnet/model_list?update=false";
#if DEBUG
            Console.WriteLine("Querying for ControlNet Models at " + modelsURI);
#endif
            WebRequest request = HttpWebRequest.Create(modelsURI);
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(responseStream);
            string responseString = streamReader.ReadToEnd();
            ControlNetAPIResponse deserializedModels = Newtonsoft.Json.JsonConvert.DeserializeObject<ControlNetAPIResponse>(responseString);
            ControlNetModel[] parsedModels = new ControlNetModel[deserializedModels.modelList.Length];
            for (int i = 0; i < parsedModels.Length; i++) {
                string foundName = deserializedModels.modelList[i].Substring(0,deserializedModels.modelList[i].Length-11);
                string foundHash = deserializedModels.modelList[i].Substring(deserializedModels.modelList[i].Length - 10);
                foundHash = foundHash.Replace("[", "").Replace("]","");
                ControlNetModule foundModule = ControlNetModule.UNKNOWN;

                //Detect Module Type
                if (foundName.ToLower().Contains("openpose")) {
                    foundModule = ControlNetModule.OPENPOSE;
                } else if (foundName.ToLower().Contains("depth")) {
                    foundModule = ControlNetModule.DEPTH;
                }

                parsedModels[i] = new ControlNetModel(foundName, foundHash, foundModule);
#if DEBUG
                Console.WriteLine(parsedModels[i].ToString()); ;
#endif
            }
            return parsedModels;
        }
        public override string ToString() {
            return "ControlNetModel: " + Name + " (Hash: "+Hash+") (Module: " + Module + ")";
        }
    }
}
