﻿using System;
using System.IO;
using System.Net;
using stableweavelib.Parameters;

namespace stableweavelib.Generators {
    public class Img2img {
        public static Image[] GenerateImage(string baseURI, ImgParameters parameters) {
            string generationURI = baseURI + "/sdapi/v1/img2img";
            Console.WriteLine("Generating image via Img2Img at" + generationURI);
            WebRequest request = HttpWebRequest.Create(generationURI);
            request.ContentType = "application/json";
            request.Method = "POST";
            string jsonParameters = parameters.getJson();

            if (parameters != null) {
                if (parameters.ControlNetParamenters.Count > 0) {
                    Console.WriteLine("ControlNet Parameters were found. Adding...");
                    string controlNetParameters = "\"alwayson_scripts\": {\"controlnet\": {\"args\": [";

                    int parametersProcessed = 0;
                    foreach (ControlNetParameters controlNetEntry in parameters.ControlNetParamenters) {
                        //Skip unmapped module types
                        if (controlNetEntry.Model.Module == ControlNetModel.ControlNetModule.UNKNOWN) {
                            continue;
                        }
                        //Add Separator
                        if (parametersProcessed > 0) {
                            controlNetParameters += ",";
                        }
                        controlNetParameters += controlNetEntry.GetJsonEntryText();
                        parametersProcessed++;
                    }

                    controlNetParameters += "]}}";
                    if (parametersProcessed > 0) {
                        jsonParameters = jsonParameters.Substring(0, jsonParameters.Length - 1) + "," + controlNetParameters + "}";
                    }
                }
            }

            Console.WriteLine("JSON Parameters:\n" + jsonParameters);
#if DEBUG
            string filePathInputJson = "./" + DateTime.Now.ToLongDateString() + "-Img2ImgInput" + ".json";
            File.WriteAllText(filePathInputJson, jsonParameters);
#endif
            using (var streamWriter = new StreamWriter(request.GetRequestStream())) {
                streamWriter.Write(jsonParameters);
            }
            request.Timeout = 600000 * parameters.BatchSize;
            WebResponse response = request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader streamReader = new StreamReader(responseStream);
            string responseString = streamReader.ReadToEnd();
#if DEBUG
            string filePathOutputJson = "./" + DateTime.Now.ToLongDateString() + "-Img2ImgResults" + ".json";
            File.WriteAllText(filePathOutputJson, responseString);
#endif
            Image[] resultingImages = ResponseParser.ParseImagesFromResponse(responseString);
            return resultingImages;
        }
    }
}
