﻿using System;
namespace stableweavelib.Parameters {
    public class ControlNetParameters {
        public Image InputImage { get; set; }
        public ControlNetModel Model { get; set; }

        public ControlNetParameters(Image image, ControlNetModel model) {
            InputImage = image;
            Model = model;
        }

        public string GetJsonEntryText() {
            string json = "{"
            + "\"input_image\": "
            + "\"" + InputImage.Base64 + "\","
            + "\"model\": "
            + "\"" + Model.Name + "\","
            + "\"module\": "
            + "\"" + Model.Module.ToString().ToLower() + "\""
            + "}";
            return json;
        }
    }
}
