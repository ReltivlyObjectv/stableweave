﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace stableweavelib.Parameters {
    public class TxtParameters : GenerationParameters {
        //Parent Properties
        [JsonProperty("batch_size")]
        public int BatchSize { get; set; }
        [JsonProperty("cfg_scale")]
        public float CfgScale { get; set; }
        [JsonProperty("height")]
        public int OutputHeight { get; set; }
        [JsonProperty("width")]
        public int OutputWidth { get; set; }
        [JsonProperty("prompt")]
        public string Prompt { get; set; }
        [JsonProperty("negative_prompt")]
        public string PromptNeg { get; set; }
        [JsonProperty("sampler_index")]
        public string SamplerName { get; set; }
        [JsonProperty("steps")]
        public int SamplingSteps { get; set; }
        [JsonProperty("seed")]
        public int Seed { get; set; }
        [JsonProperty("save_images")]
        public bool SaveImages { get; set; }
        [JsonIgnore]
        public LinkedList<ControlNetParameters> ControlNetParamenters { get; set; } = new LinkedList<ControlNetParameters>();

        public TxtParameters() {
            BatchSize = 1;
            CfgScale = 8.5f;
            OutputHeight = 512;
            OutputWidth = 512;
            Prompt = "";
            PromptNeg = "";
            SamplerName = "DPM++ SDE Karras";
            SamplingSteps = 40;
            Seed = -1;
            SaveImages = true;
        }
        public string getJson() {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
