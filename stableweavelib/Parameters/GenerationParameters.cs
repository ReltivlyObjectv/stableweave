﻿using System;
using System.Collections.Generic;

namespace stableweavelib.Parameters {
    public interface GenerationParameters {
        public int BatchSize { get; set; }
        public float CfgScale { get; set; }
        public int OutputHeight { get; set; }
        public int OutputWidth { get; set; }
        public string Prompt { get; set; }
        public string PromptNeg { get; set; }
        public string SamplerName { get; set; }
        public int SamplingSteps { get; set; }
        public int Seed { get; set; }
        public LinkedList<ControlNetParameters> ControlNetParamenters { get; set; }
    }
}
