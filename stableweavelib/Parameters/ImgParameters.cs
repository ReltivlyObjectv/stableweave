﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace stableweavelib.Parameters {
    public class ImgParameters : GenerationParameters {
        //Parent Properties
        [JsonProperty("batch_size")]
        public int BatchSize { get; set; }
        [JsonProperty("cfg_scale")]
        public float CfgScale { get; set; }
        [JsonProperty("height")]
        public int OutputHeight { get; set; }
        [JsonProperty("width")]
        public int OutputWidth { get; set; }
        [JsonProperty("prompt")]
        public string Prompt { get; set; }
        [JsonProperty("negative_prompt")]
        public string PromptNeg { get; set; }
        [JsonProperty("sampler_index")]
        public string SamplerName { get; set; }
        [JsonProperty("steps")]
        public int SamplingSteps { get; set; }
        [JsonProperty("seed")]
        public int Seed { get; set; }
        [JsonProperty("save_images")]
        public bool SaveImages { get; set; }
        [JsonProperty("init_image")]
        public string[] originalImageBase64 {
            get {
                return new string[] { OriginalImage.Base64 };
            }
        }
        [JsonProperty("denoising_strength")]
        public float DenoisingStrength { get; set; }
        [JsonProperty("resize_mode")]
        public int ResizeMode { get; set; }

        [JsonIgnore]
        public Image OriginalImage { get; set; }
        [JsonIgnore]
        public LinkedList<ControlNetParameters> ControlNetParamenters { get; set; } = new LinkedList<ControlNetParameters>();

        //Img Properties
        public enum ResizeModes {
            JUST_RESIZE, //0
            CROP_AND_RESIZE, //1
            RESIZE_AND_FILL, //2
            //JUST_RESIZE_LATENT
        }
        public ResizeModes getResizeMode() {
            switch (ResizeMode) {
                case 0:
                    return ResizeModes.JUST_RESIZE;
                case 1:
                    return ResizeModes.CROP_AND_RESIZE;
                case 2:
                    return ResizeModes.RESIZE_AND_FILL;
            }
            return ResizeModes.RESIZE_AND_FILL;
        }
        public void setResizeMode(ResizeModes mode) {
            switch (mode) {
                case ResizeModes.JUST_RESIZE:
                    ResizeMode = 0;
                    break;
                case ResizeModes.CROP_AND_RESIZE:
                    ResizeMode = 1;
                    break;
                case ResizeModes.RESIZE_AND_FILL:
                    ResizeMode = 2;
                    break;
            }
        }
        public ImgParameters(Image originalImage) {
            BatchSize = 1;
            CfgScale = 8.5f;
            OutputHeight = 512;
            OutputWidth = 512;
            Prompt = "";
            PromptNeg = "";
            SamplerName = "DPM++ 2M Karras";
            SamplingSteps = 40;
            Seed = -1;
            DenoisingStrength = .75f;
            setResizeMode(ResizeModes.RESIZE_AND_FILL);
            OriginalImage = originalImage;
            SaveImages = true;
        }
        public string getJson() {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
