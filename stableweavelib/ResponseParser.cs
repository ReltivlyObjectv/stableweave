﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;

namespace stableweavelib {
    public class ResponseParser {
        public static Image[] ParseImagesFromResponse(string responseString) {
            JObject parsedResponse = JObject.Parse(responseString);
            JArray responseJsonImages = (JArray) parsedResponse["images"];
            Image[] parsedImages = new Image[responseJsonImages.Count];
            for (int i = 0; i < responseJsonImages.Count; i++) {
                Image parsedImage = new Image(parsedResponse["images"][i].ToString());
                parsedImages[i] = parsedImage;
            }
            return parsedImages;
        }
    }
}
